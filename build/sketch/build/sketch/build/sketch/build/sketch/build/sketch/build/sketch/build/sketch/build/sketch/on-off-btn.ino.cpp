#include <Arduino.h>
#line 1 "/home/zeb/dev/jeopardy-btn/on-off-btn.ino"
#line 1 "/home/zeb/dev/jeopardy-btn/on-off-btn.ino"
/* * * * * * * * * * * * * * * *
  Jeopardy Button WeMos D1 mini  
  Author: zeb (sebastian@zebweb.dk)
* * * * * * * * * * * * * * * */

// INCLUDES
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>

// Pin and time vars

int inPin = D2;     // button pin
int outPin = D5;    // led pin

int state = HIGH;
int reading;
int previous = LOW;
int ledState = LOW;

unsigned long previousMillis = 0; 
const long interval = 1000; 

long currtime = 0;
long debounce = 500;

// Application states

enum States { READY, ON, OFF };
States programState = READY;

// Wifi
const char* ssid     = "ZebNet5";
const char* password = "z3bn3th0m13";
ESP8266WiFiMulti WiFiMulti;
WebSocketsClient webSocket;

const char* buttonId = "BTN_1";

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

	switch(type) {
		case WStype_DISCONNECTED:
			Serial.printf("[WSc] Disconnected!\n");
			break;
		case WStype_CONNECTED: {
			Serial.printf("[WSc] Connected to url: %s\n", payload);            
			// send message to server when Connected			
            blinkIndicator(100);      
            state = ON;      
		}
			break;
		case WStype_TEXT:			
            String cmd = (char*)payload;
            Serial.println(cmd);
            if(cmd == "READY"){
                state = READY;
            }

            if(cmd == "OFF"){
                Serial.println("SWITCHING OFF");
                state = OFF;
            }
			// send message to server
			// webSocket.sendTXT("message here");
			break;		
	}

}

void setup()
{
  Serial.begin(115200);

  // setup pin modes for button and LED pins
  pinMode(inPin, INPUT_PULLUP);
  pinMode(outPin, OUTPUT); 

  digitalWrite(outPin, HIGH); // to indicate we are now searching for wifi
  // connect to wifi
  WiFiMulti.addAP(ssid, password);

	//WiFi.disconnect();
	while(WiFiMulti.run() != WL_CONNECTED) {
		delay(100);
	}
    blinkIndicator(300);
    Serial.printf("Connected to wifi, connecting WS");
    digitalWrite(outPin, HIGH);

    // server address, port and URL
	webSocket.begin("192.168.0.30", 3000, "/ws/echo?id=btn1");
    
	// event handler
	webSocket.onEvent(webSocketEvent);

    webSocket.setReconnectInterval(1000);
}

void loop()
{
    webSocket.loop();
    // Button Logic
    reading = digitalRead(inPin);  
    if (reading == HIGH && previous == LOW && millis() - currtime > debounce) {
        toggleButton();
        currtime = millis();    
    }
    previous = reading;

    // State machine
    switch(programState) {
        case READY:
            breathe();
        break;
        case ON:
            digitalWrite(outPin, HIGH);
        break;
        case OFF:
            digitalWrite(outPin, LOW);
        break;
    }
}

void breathe(){
    unsigned long currentMillis = millis();
        if (currentMillis - previousMillis >= interval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;

        // if the LED is off turn it on and vice-versa:
        if (ledState == LOW) {
        ledState = HIGH;
        } else {
        ledState = LOW;
        }

        // set the LED with the ledState of the variable:
        digitalWrite(outPin, ledState);
    }
}

void toggleButton(){
    webSocket.sendTXT("CLICK");
    /*if(programState == ON){
        programState = READY;
    } else if(programState == READY){
        programState = OFF;
    } else if(programState == OFF){
        programState = ON;
    }*/
}

void blinkIndicator(int delayTime){
    digitalWrite(outPin, HIGH);
    delay(delayTime);
    digitalWrite(outPin, LOW);
    delay(delayTime);
    digitalWrite(outPin, HIGH);
    delay(delayTime);
    digitalWrite(outPin, LOW);
    delay(delayTime);
    digitalWrite(outPin, HIGH);
    delay(delayTime);
    digitalWrite(outPin, LOW);            
}
